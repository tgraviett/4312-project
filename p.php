<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/7/13
 * Time: 8:37 PM
 */
require_once'core/init.php';
$db = DB::getInstance();

if(!Input::get('party')){
    Session::flash('home', 'Please enter a party code!');
    Redirect::to('join.php');
} else {
    //set the party code
    $code = strtoupper(Input::get('party'));

    //instantiate instance of Party Class
    $party = new Party();

    if(!$party->find($code)){
        Session::flash('error', 'Please enter a valid party code.');
        Redirect::to('join.php');
    } else {
        $user = new User();


        if(!$user->isLoggedIn()){
            //anonymous guest user
            $hash = Hash::unique();
            $hashCheck = $db->get('anon_session', array('hash','=', $hash));
            if(!$hashCheck){
                $db->insert('anon_session', array(
                    'public_event_id' => $code,
                    'hash' => $hash,
                ));
                Cookie::put(Config::get('remember/cookie_name'),$hash, Config::get('remember/cookie_expiration'));
            }

            $title = 'Title';
            include 'templates/header.php';
            ?>
            </head>
            <body>
            <div data-role="page" id="preparty">
                <div data-role="header" data-position="fixed">
                    <h1>Party Time!</h1>
                    <p class="spacer50"></p>
                    <div class="button" style="font-size:2em" onclick="<?php echo "window.location.href='search.php?party=". $code ."';";?>">
                        Add Music
                    </div>
                </div>
                <div data-role="content">
                    <div class="tracklist">
                        <?php
                        $songs = new Music();
                        $songlist = $songs->findAllMusic($code);

                        if(!$songslist){
                            echo "No songs yet, why don't you add some!";
                        }else {
                            foreach($songslist as $s){
                        ?>

                        <div class="trackname"><?php echo $s->data()->title; ?></div>
                            <div class="track">
                                <ul>
                                    <li><div class="upvote"><div class="uparrow">&#xf0d8;</div></div>
                                    <li><div class="downvote"><div class="downarrow">&#xf0d7;</div></div></li><p></p>
                                </ul>
                            </div>
                        </div>
                        <?php
                            }
                        }
                        ?>
                </div>
            </body>
            <script>
                $(".uparrow").click(function(){
                    var songinfo = $(this).closest('.uparrow').data();
                    var data ={
                        'URI': songinfo.uri,
                        'duration': songinfo.duration,
                        'title': songinfo.title,
                        'event_ID': '<?php echo $code;?>'
                    };
                    alert(data);
                    $.ajax({
                        type: "POST",
                        url: 's.php',
                        data: {
                            info:data,
                            action: 'addSong'
                        },
                        success: function(data){
                            alert(data);/*window.location.replace("p.php?party=<?php //echo $code;?>")*/
                        }
                    });
                });

            </script>
            </html>
            <?php


        } else if($user->data()->ID != $party->data()->creators_ID) {
            $title = $code . ' - '.$party->title;
            ?>
            </head>
                <body>
                <div data-role="page" id="preparty">
                    <div data-role="header" data-position="fixed">
                        <h1><?php echo $party->data()->Public_Event_ID .' - '.$party->data()->Title ?></h1>
                        <p class="spacer50"></p>
                        <div class="button" style="font-size:2em" onclick="<?php echo "window.location.href='search.php?party=". $code ."';";?>">
                            Add Music
                        </div>
                    </div>
                    <div data-role="content">
                        <div class="tracklist">
                            <?php
                            $songs = new Music();
                            $songlist = $songs->findAllMusic($code);

                            if(!$songslist){
                                echo "No songs yet, why don't you add some!";
                            }else {
                                foreach($songslist as $s){
                                    ?>

                                    <div class="trackname"><?php echo $s->data()->Title; ?></div>
                                        <div class="track">
                                            <ul>
                                                <li><div class="upvote"><div class="uparrow">&#xf0d8;</div></div>
                                                <li><div class="downvote"><div class="downarrow">&#xf0d7;</div></div></li><p></p>
                                            </ul>
                                        </div>
                                    </div>
                                <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </body>
            <script>
                $(".plus").click(function(){
                    var songinfo = $(this).closest('.plus').data();
                    var data ={
                        'URI': songinfo.uri,
                        'duration': songinfo.duration,
                        'title': songinfo.title,
                        'event_ID': '<?php echo $code;?>'
                    };
                    alert(data);
                    $.ajax({
                        type: "POST",
                        url: 's.php',
                        data: {
                            info:data,
                            action: 'addSong'
                        },
                        success: function(data){
                            alert(data);/*window.location.replace("p.php?party=<?php //echo $code;?>")*/
                        }
                    });
                });

            </script>
        </html>
        <?php

        } else {
        /*for the host*/

            $title = 'Jukebocracy Playlist';
            include 'templates/header.php'
            ?>
            <script>
                SC.initialize({
                    client_id: '50f0c94c512289d70a2ef655ad093ee2'
                });

                SC.get("/tracks/81815566", {}, function(sound){
                    $("#audio-test").attr("src", sound.stream_url+"?client_id=50f0c94c512289d70a2ef655ad093ee2");
                });
            </script>
            </head>
            <body>
                <div data-role="page">
                    <div data-role="header" data-position="fixed"><p></p>
                        <h5>Admin Party Panel</h5>
                        <h1><?php echo $party->data()->Public_Event_ID .' - '.$party->data()->Title ?></h1><p><p>
                    <audio id="audio-test" controls></audio>
                    <p class="spacer50"></p>
                    <div class="button" style="font-size:2em" onclick="<?php echo "window.location.href='search.php?party=". $code ."';";?>">Add Music</a></div>
                    </div>
                        <div data-role="content">
                            <div class="tracklist">
                                <?php
                                    $songs = new Music();
                                    $songlist = $songs->findAllMusic($code);

                                    if(count($songlist)<0){
                                        echo "No songs yet, why don't you add some!";
                                    }else {
                                        foreach($songlist as $s){
                                        ?>

                                    <div class="trackname"><?php echo $s->title; ?></div>
                                        <div class="track">
                                            <ul>
                                                <li><div class="upvote"><div class="uparrow" data-id="<?php echo $s->SONG_ID; ?>" data-count="0">&#xf0d8;</div></div></li>
                                                <li><div class="delete"><div class="cross" data-id="<?php echo $s->SONG_ID; ?>">&#xf00d;</div></div></li>
                                                <li><div class="downvote"><div class="downarrow" data-id="<?php echo $s->SONG_ID; ?>" data-count="0">&#xf0d7;
                                                        </div></div></li><p></p>
                                            </ul>
                                        </div>
                                    </div>
                                <?php
                            }
                        }
                        ?>
                        </div>
                    </div>
                </body>
                <script>
                    $(".uparrow").click(function(){

                        if($(this).data('count') < 1){
                            alert ('hi');
                            $.ajax({
                                type: "POST",
                                url: 's.php',
                                data: {
                                    id: $(this).closest('.uparrow').data('id'),
                                    action: upvote,
                                    uid: '<?php echo $user->data()->ID; ?>'
                                },
                                success: function(data){
                                    if(data == 'success'){
                                    $this.parent().css("background", "#339966");
                                    $this.data().count++;
                                    } else {
                                        alert('There was an error voting!');
                                    }
                                }
                            }
                        }
                    });
                    /*$(".downarrow").click(function(){
                        var id = $(this).closest('.downarrow').data('id');
                        if(data('count') < 0){
                            $.ajax({
                                type: "POST",
                                url: 's.php',
                                data: {
                                    id: id,
                                    action: 'downvote',
                                    uid: '<?php //echo $user->data()->id; ?>

                                },
                                success: function(data){
                                    if(data == 'success'){
                                    $(this).parent().css("background", "#A42626");
                                    $(this).data().count++;
                                    } else {
                                        alert('There was an error voting!');
                                    }
                                }
                            }*/
                        }
                    });

                </script>
               </html>
        <?php
        }
    }
}
?>
