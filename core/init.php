<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:07 AM
 */
session_start();

$GLOBALS['config'] = array(
    'mysql' => array(
        'host' => '127.0.0.1',
        'username' => 'root',
        'password' => 'kittens',
        'db' => 'jukebocracydb'
    ),
    'remember' =>array(
        'cookie_name' => 'hash',
        'cookie_expiration' => 86400,
    ),
    'session' => array(
        'session_name' => 'ID',
        'token_name' => 'token'
    )
);

function my_autoloader($class){
    include 'classes/'.$class.'.php';
}

spl_autoload_register('my_autoloader');

require_once 'functions/sanitize.php';

if(Cookie::exists(Config::get('remember/cookie_name')) && !Session::exists(Config::get('session/session_name'))) {
    $hash = Cookie::get(Config::get('remember/cookie_name'));
    $hashCheck = DB::getInstance()->get('users_session', array('hash', '=', $hash));

    if($hashCheck->count()){
        //if($hashCheck->data()->anon != 1){
        $user = new User($hashCheck->first()->user_id);
        $user->login();
    //}
    }
}