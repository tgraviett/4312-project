<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/results()13
 * Time: 1:05 AM
 */
require_once 'core/init.php';
include 'templates/header.php';
if(Session::exists('error')){
   echo '<div class="error">' .Session::flash('error').'</div>';
}

$user = new User();
if ($user->isLoggedIn()){
    $party = new Party();
    ?>
</head>
<body>
    <div>
        <p>Hello <a href="profile.php?user=<?php echo escape($user->data()->email);?>"><?php echo escape($user->data
                    ()->email);?></a>! &nbsp;&nbsp;&nbsp;&nbsp;
            <a href="manageaccount.php">Manage account</a> | <a href="logout.php">Log Out</a>
        </p>
    </div>
    <div data-role="header">
        <h1>Party Management</h1>
        <br>
        <?php if(Session::exists('error')){
            echo '<div class="error">' .Session::flash('error').'</div>';
        }?>
        <br>
    </div>
    <div class="listtextmedium">
        <ul>
        <?php
        $parties = $party->findPartiesHosted($user->data()->ID);
        if(count($parties)>0){
            foreach($parties as $p){
                ?>
                 <li><a href="p.php?party=<?php echo $p->Public_Event_ID; ?>"><?php echo $p->Title;
                         ?></a> -<?php echo $p->Public_Event_ID; ?><br>
                 Start Date: <?php echo $p->Start_Date; ?> | End date: <?php echo $p->End_Date; ?>
                 </li>
                <br>
                <?php
            }
        } else {
            ?>
            <li><h4>You haven't created any parties yet!</h4></li>
            <P class="spacer50"></P>
            <P class="spacer50"></P>
            <P class="spacer50"></P>
            <?php
        }
        ?>

            <br>
        </ul>
    </div>
    <ul>
        <li>
            <div class="button" onclick="window.location.href='startparty.php';" >create</div>
        </li>
    </ul>
</body>
<?php

    if($user->hasPermission('admin')){
        echo '<p>You are an administrator!</P>';
    }
} else {?>
    </head>
    <body>
    <div data-role="page" id="index">
        <div data-role="header">
            <h1>Muzik Libre</h1>
            <h6 style="text-align:center">we are a social jukebox that helps
                deliver musical justice for the people, by the people<h6>
                    <p class="spacer"></p>
        </div>

        <div data-role="content">
            <ul>
                <li>
                    <div class="button" onclick="window.location.href='join.php';">Join the Party</div>
                </li>
                <br>
                <li>
                    <div class="button2" onclick="window.location.href='startparty.php';">Create a Party</div>
                </li>
                <br>
                <li>
                    <div class="button2" onclick="window.location.href='howtoparty.php'">Learn to Party</div>
                </li>
            </ul>
        </div>
    </body>
</html>

<?php

}
