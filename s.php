<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/8/13
 * Time: 9:26 PM
 */

require_once 'core/init.php';

$db = DB::getInstance();
$user = new User();

if(Input::exists()){
    if(Input::get('action')=='addSong'){
        $data = Input::get('info');
        if($db->insert('songs', $data)){
            echo 'success';
        } else {
            print_r($data);
        }
    } else if(Input::get('action')=='downvote'){
        $id = Input::get('id');
        $user_ID = Input::get('uid');
        $votes = $db->get('users', array('ID', '=', $user_ID));
        $vote = unserialize($user->data()->down_votes);
        $vote[] = $id;
        $data = array(
            'down_votes' => serialize($vote)
        );
        if($db->update('users', $user_ID, $data)){
            echo 'success';
        } else {
            echo'fail';
        }
    } else if(Input::get('action')=='upvote'){
        $id = Input::get('id');
        $user_ID = Input::get('uid');
        $votes = $db->get('users', array('ID', '=', $user_ID));
        $vote = unserialize($user->data()->up_votes);
        $vote[] = $id;
        $data = array(
            'down_votes' => serialize($vote)
        );
        if($db->update('users', $user->data()->ID, $data)){
            echo 'success';
        } else {
            echo'fail';
        }
    }
}
