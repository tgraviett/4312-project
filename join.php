<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/8/13
 * Time: 4:07 AM
 */
require_once 'core/init.php';
$title = 'Join the party!';
include_once 'templates/header.php';

if(isset($_POST['public_event_id'])){
    $party = Input::get('public_event_id');
    echo $party;
    Redirect::to('p.php?party='. $party);
}

?>
</head>

<body>

<div data-role="page" id="join">
    <div data-role="header">
        <br>
        <h1>Let's Liberate</h1>
    </div>
    <br>
    <div id="error">
        <?php
            if(Session::exists('error')){
               echo '<p>' . Session::flash('error') . '</p>';
            }
        ?>
    </div>
    <div data-role="content">
        <form action="" method="POST">
                <input id="EventID" class="inputLarge" name="public_event_id" type="text" placeholder="Paste Code Here" >
                <br>
                <input type="submit" class="button3" value="Join the Party&nbsp;>">
        </form>

        <br>
        <br>

        <div class="button2" onclick="window.location.href='login.php';">
            Manage Party &nbsp;&nbsp; >
        </div>

    </div>

</div>


</body>
</html>