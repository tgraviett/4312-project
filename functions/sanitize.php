<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:07 AM
 */

function escape($string){
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}