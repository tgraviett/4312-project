SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `jukebocracydb` ;
CREATE SCHEMA IF NOT EXISTS `jukebocracydb` DEFAULT CHARACTER SET utf8 ;
USE `jukebocracydb` ;

-- -----------------------------------------------------
-- Table `jukebocracydb`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`users` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`users` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `password` VARCHAR(64) NOT NULL ,
  `status` INT(11) NOT NULL DEFAULT '0' ,
  `events_hosted` INT(11) NOT NULL DEFAULT '0' ,
  `email` VARCHAR(255) NOT NULL ,
  `salt` VARCHAR(32) NOT NULL ,
  `joined` DATETIME NOT NULL ,
  `group` INT(11) NOT NULL ,
  `votes_up` TEXT NULL ,
  `votes_down` TEXT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`events` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`events` (
  `ID` INT(11) NOT NULL ,
  `creators_ID` INT(11) NOT NULL ,
  `Start_Date` VARCHAR(20) NOT NULL ,
  `End_Date` VARCHAR(20) NOT NULL ,
  `Event_Status` INT(11) NOT NULL DEFAULT '1' ,
  `Title` VARCHAR(150) NULL DEFAULT NULL ,
  `Public_Event_ID` VARCHAR(6) NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) ,
  INDEX `fk_EVENT_USERS_idx` (`creators_ID` ASC) ,
  CONSTRAINT `fk_EVENT_USERS`
    FOREIGN KEY (`creators_ID` )
    REFERENCES `jukebocracydb`.`users` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`groups` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`groups` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(20) NOT NULL ,
  `permissions` TEXT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`songs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`songs` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`songs` (
  `SONG_ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `event_ID` INT(11) NOT NULL ,
  `URI` VARCHAR(255) NOT NULL ,
  `song_name` VARCHAR(120) NOT NULL ,
  `length_in_ms` INT(11) NOT NULL ,
  `votes` INT(11) NOT NULL DEFAULT '0' ,
  `song_status` INT(11) NOT NULL DEFAULT '1' ,
  `play_count` INT NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`SONG_ID`) ,
  INDEX `fk_SONGS_EVENTS1_idx` (`event_ID` ASC) ,
  CONSTRAINT `fk_SONGS_EVENTS1`
    FOREIGN KEY (`event_ID` )
    REFERENCES `jukebocracydb`.`events` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`hosted_events`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`hosted_events` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`hosted_events` (
  `USERS_USER_ID` INT(11) NOT NULL ,
  `EVENTS_EVENT_ID` INT(11) NOT NULL ,
  PRIMARY KEY (`USERS_USER_ID`, `EVENTS_EVENT_ID`) ,
  INDEX `fk_table1_EVENTS1_idx` (`EVENTS_EVENT_ID` ASC) ,
  CONSTRAINT `fk_table1_EVENTS1`
    FOREIGN KEY (`EVENTS_EVENT_ID` )
    REFERENCES `jukebocracydb`.`events` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_table1_USERS1`
    FOREIGN KEY (`USERS_USER_ID` )
    REFERENCES `jukebocracydb`.`users` (`ID` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`users_session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`users_session` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`users_session` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `hash` VARCHAR(64) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `jukebocracydb`.`anon_session`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `jukebocracydb`.`anon_session` ;

CREATE  TABLE IF NOT EXISTS `jukebocracydb`.`anon_session` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT ,
  `hash` INT(11) NOT NULL ,
  `public_event_id` VARCHAR(5) NOT NULL ,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `up_votes` TEXT NULL ,
  `down_votes` TEXT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

USE `jukebocracydb` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
