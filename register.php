<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 12:40 AM
 */
require_once 'core/init.php';

if(Input::exists()){
    if(Token::check(Input::get('token'))){
       $validate = new Validate();
       $validation = $validate->check($_POST, array(
           'email'=> array(
               'required' => true,
               'min'=> 2,
               'max'=> 255,
               'unique' => 'users',
               'valid_email' => true,
           ),
           'password'=> array(
               'required' => true,
               'min'=> 6
           ),
           'password2'=> array(
               'required' => true,
               'matches' => 'password'
           )
       ));
        if($validation->passed()){
            //register user

            $user = new User();
            $salt = Hash::salt(32);

            try {

                $user->create(array(
                    'password' => Hash::make(Input::get('password'), $salt),
                    'status' => '0',
                    'events_hosted' => '0',
                    'email' => Input::get('email'),
                    'joined' => date('Y-m-d H:i:s'),
                    'salt' => $salt,
                    'group' => 1,

                ));

                Session::flash('error', 'You have been registered and can now login!');
                Redirect::to('login.php');

            }catch(Exception $e){
                die($e->getMessage());
            }
        }else{
            //output errors
            foreach($validation->errors() as $error){
                echo $error, "<br>";

            }
        }
    }
}
include 'templates/header.php';
?>
    </head>
    <body>
        <div data-role="page" id="createlogin">
            <div data-role="header">
                <h1>Create An Account</h1>
                <p class="spacer"></p>
            </div>

            <div data-role="content">
            <form action="" method="POST">
                <input type="text" name="email" placeholder="E-mail" value="<?php echo escape(Input::get('email'));?>"
                       autocomplete="off"><br>
                <br>
                <input type="password" name="password" placeholder="Enter Password" autocomplete="off" ">
                <br>
                <input type="password" name="password2" placeholder="Confirm Password" autocomplete="off" ><br>
                <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
                <input type="submit" class="button3" value="Register">
            </form>
        </div>
    </body>
</html>