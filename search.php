<?php
require_once 'core/init.php';
include_once 'templates/header.php';

if(!Input::get('party')){
    Session::flash('error', "Please join a party.");
    Redirect::to('index.php');
} else{
    $code = Input::get('party');
}

?>

</head>
<body>
<div data-role="page" id="search">
    <div data-role="header" data-position="fixed"><p></p>
        <h1>Find Music - <?php echo $code;?></h1><p><p>
        <p class="spacer50"></p>
        <form action="" method="POST">
            <input type="text" placeholder="Search for Music" name="trackname"></br>
            <input type="submit" class="button3" value="Confirm Selection">
        </form>

    </div><p></p>
    <div data-role="content">
        <div class="tracklist">
            <?php
            if(!isset($_POST['trackname'])){
                $trackname = '';
            } else {
                $trackname = $_POST['trackname'];

                //soundcloud url for api access. this returns an xml search
                $url = 'https://api.soundcloud.com/tracks?client_id=50f0c94c512289d70a2ef655ad093ee2&q='. urlencode($trackname);

                $xml = simplexml_load_file($url);
                foreach ($xml->track as $t){
                    $title = $t->title;
                    $duration = $t->duration;
                    $strip = "https://api.soundcloud.com/tracks/";
                    $uri = str_replace($strip, "",$t->uri);

                    ?>
                    <div class="track">

                        <div class="trackname" ><?php echo $title; ?></div>
                        <div class="addmusic">
                            <div class="plus"  data-duration="<?php echo $duration; ?>" data-uri="<?php echo $uri; ?>"
                                 data-title="<?php echo $title;?>">
                                &#xf067;
                            </div>

                        </div>
                        <br>
                    </div>
                    <hr>
                <?php
                }
            }
            ?>
        </div>
    </div>
</div>
<script>
    $(".plus").click(function(){
        var songinfo = $(this).closest('.plus').data();
        var data ={
            'URI': songinfo.uri,
            'duration': songinfo.duration,
            'title': songinfo.title,
            'event_ID': '<?php echo $code;?>'
        };
        $.ajax({
            type: "POST",
            url: 's.php',
            data: {
                info:data,
                action: 'addSong'
            },
            success: function(data){
                window.location.replace("p.php?party=<?php echo $code;?>")
            }
        });
    });

</script>

</body>
</html>