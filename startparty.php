<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/2/13
 * Time: 1:38 PM
 */
require_once 'core/init.php';
$title = 'Start a Party';

$user = new User();


if(!$user->isLoggedIn()){
    Redirect::to('login.php');
} else{
    if(Input::exists()){
        if(Token::check(Input::get('token'))){
            //validate form
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'startdate' => array(
                    'required' => true,
                    'valid_date' => true
                ),
                'enddate' => array(
                    'required' => true,
                    'valid_date' => true,
                    //'valid_end_date' => 'startdate',
                ),
                'title' => array(
                    'required' => true,
                )
            ));

            if($validation->passed()){

                $party = new Party();
                $code = $party->uniqueCode();
                try {
                    $party->create(array(
                        'creators_ID' => $user->data()->ID,
                        'Start_Date' => Input::get('startdate'),
                        'End_Date' => Input::get('enddate'),
                        'Title' => Input::get('title'),
                        'Public_Event_ID' => $code,
                    ));
                } catch(Exception $e){
                    die(Session::flash($e->getMessage()));
                }
                Redirect::to('p.php?party='.$code);
            }else {
                foreach($validation->errors() as $error){
                    echo $error, "<br>";
                }
            }
        }
    }
}
include 'templates/header.php';

?>
</head>

<body>
<div data-role="page" id="create">
    <div data-role="header">
        <h1>Let's Liberate</h1>
        <p class="spacer50"></p>
        <?php if(Session::exists('error')){
            echo '<div class="error">' .Session::flash('error').'</div>';
        }?>
    </div>

    <div data-role="content">
        <form action="" method="POST">
            <input type="text" placeholder="Name Your Party" name="title"></br>
            <input type="text" name="startdate" id="startdate" placeholder="Start Date of Party"
                   autocomplete="off"></br>
            <input type="text" name="enddate" id="enddate" autocomplete="off" placeholder="End Date of Party">
            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
            <p class="spacer"></p>
            <input type="submit" class="button3" value="Create your Party">
        </form>
    </div>
    <script>
        $(function() {
            $( "#startdate" ).datepicker({
                changeMonth: true,
                minDate: 0,
                onSelect: function(){
                    var startDate = $( "#startdate" ).datepicker( "getDate" );
                    var endDate = new Date();
                    endDate.setDate(startDate.getDate() + 2);
                    $( "#enddate" ).datepicker( "setDate" , endDate );
                },
                onClose: function( selectedDate ){
                    $( "#enddate" ).datepicker( "option", "minDate", selectedDate );
                }

            });
            $( "#enddate" ).datepicker({
                changeMonth: true,
                onClose: function( selectedDate ){
                    $( "#startdate" ).datepicker("option", "maxDate", selectedDate );
                }
            });
        });
    </script>
</body>
</html>