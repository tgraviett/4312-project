<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/1/13
 * Time: 9:52 PM
 */
require_once 'core/init.php';

$user = new User();
$user->logout();

Redirect::to('index.php');