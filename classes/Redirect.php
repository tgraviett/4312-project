<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:05 AM
 */
class Redirect{
    public static function to($location = null) {
    if($location) {
        if(is_numeric($location)){
            switch($location){
                case 404:
                header('HTTP/1.0 404 Not Found');
                include 'includes/errors/404.php';
                exit();
                break;
            }
        }
        header('Location:'. $location);
        exit();
    }
}
}