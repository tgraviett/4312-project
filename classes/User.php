<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:06 AM
 */
class User {
    private $_db,
            $_data,
            $_sessionName,
            $_cookieName,
            $_isLoggedIn;

    public function __construct($user = null) {
        $this->_db = DB::getInstance();
        $this->_sessionName = Config::get('session/session_name');
        $this->_cookieName = Config::get('remember/cookie_name');

        if(!$user) {
            if(Session::exists($this->_sessionName)){
                $user = Session::get($this->_sessionName);
                if($this->find($user)){
                    $this->_isLoggedIn = true;
                } else {

                }
            }
        } else  {
            $this->find($user);
        }
    }

    public function create($fields=array()) {
        if(!$this->_db->insert('users', $fields)){
            throw new Exception('There was a problem creating an account.');
        }
    }

    public function find($user = null){
        if($user){
            $field = (is_numeric($user)) ? 'ID' : 'email';
            $data = $this->_db->get('users', array($field, '=', $user));

            if ($data->count()){
                $this->_data = $data->first();
                return true;
            }
        }
        return false;

    }

    public function login($email = null, $password=null, $remember=false){

        if(!$email && !$password && $this->exists()){
            Session::put($this->_sessionName, $this->data()->ID);

        } else {
            $user = $this->find($email);

            if($this->data()->password === Hash::make($password,$this->data()->salt)){
                Session::put($this->_sessionName, $this->data()->ID);

                if($remember){
                    $hash = Hash::unique();
                    $hashCheck = $this->_db->get('users_session', array('user_id','=', $this->data()->ID));


                    if(!$hashCheck->count()) {
                        $this->_db->insert('users_session', array(
                            'user_id' => $this->data()->ID,
                            'hash'=>$hash,
                            'anon'=>'0'
                            ));
                    } else {
                        $hash = $hashCheck->first()->has;
                    }

                    Cookie::put($this->_cookieName,$hash, Config::get('remember/cookie_expiration'));
                }
                return true;
            }
        }
        return false;
    }

    public function exists(){
        return (!empty($this->_data))? true : false;
    }

    public function logout() {

        $this->_db->delete('users_session', array('id','=',$this->data()->ID));

        Cookie::delete($this->_cookieName);
        Session::delete($this->_sessionName);
    }

    public function data(){
        return $this->_data;
    }

    public function update($fields = array(), $ID = null){

        if(!$ID && $this->isLoggedIn()){

            $ID = $this->data()->ID;
        }

        if(!$this->_db->update('users', $ID, $fields)){
            var_dump($this->_db->update('users', $ID,$fields));
            throw new Exception('There was a problem updating.');
        }
    }

    public function hasPermission($key){
        $group = $this->_db->get('groups', array('id', '=', $this->data()->group));
        if($group->count()){
            $permissions = json_decode($group->first()->permissions, true);

            if($permissions[$key]== true){
                return true;
            }

        }return false;
    }

    public function isLoggedIn(){
        return $this->_isLoggedIn;
    }

}