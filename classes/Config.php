<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:02 AM
 */
/** @noinspection PhpInconsistentReturnPointsInspection */
class Config {
    public static function get($path = null) {
        if($path) {
            $config =  $GLOBALS['config'];
            $path = explode('/', $path);

            foreach($path as $part) {
                if(isset($config[$part])){
                $config = $config[$part];
                }
            }
        return $config;
        }
    return false;
    }
}