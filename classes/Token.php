<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:05 AM
 */
class Token{
    public static function generate(){
        return Session::put(Config::get('session/token_name'), md5(uniqid()));
    }

    public static function check($token){
        $tokenName = Config::get('session/token_name');

        if(Session::exists($tokenName) && $token === Session::get($tokenName)){
            Session::delete($tokenName);
            return true;
        }

        return false;
    }

}