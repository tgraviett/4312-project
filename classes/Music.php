<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/2/13
 * Time: 1:06 PM
 */
class Music {
    private $_db,
            $_data;

    public function __construct(){
        $this->_db = DB::getInstance();
    }

    public function findAllMusic($code = null){
        $search = $this->_db->get('songs', array('event_ID','=', $code));

        if($search){
            return $search->results();
        }
        return false;
    }

    public function songVote($id = null){
        $votes = $this->_db->get('songs', array('SONG_ID','=', $id ));

        if($votes){
            return $votes->results();
        }
        return false;
    }

    public function uniqueSong($uri = null){
        if($uri){
            $data = $this->_db->get('music', array('uri', '=', $uri));

            if(!$data->count()){
                return true;
            }
            return false;
        }
        Session::flash('error','There was an error in submitting the song');
        return false;
    }

    public function addSong($song_info = array()){
        if(!$this->_db->insert('songs',$song_info)){
            throw new Exception('There was a problem adding the song.');
        }
    }




}