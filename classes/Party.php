<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/2/13
 * Time: 4:43 AM
 */
class Party {
    private $_db,
            $_data,
            $_results,
            $_count;

    public function __construct($code = null) {
        $this->_db = DB::getInstance();

    }

    public function createEvent($Start_Date = null, $End_Date = null, $Title = null, $Code = null){


    }

    private static function generateCode(){

        $cycle = 5;      //how many characters
        $postart = 0;    //random's offset start position
        $keymade = "";   //starting value for resulting key
        $array = array("2","3","4","5","6","7","8","9","A","B","C","D","E","F","G","H","J","K","M","N","P","Q","R","S","T","U","V","W","X","Y","Z");
        shuffle($array); //one more degree of randomization
        $randmake = array_rand($array, $cycle);  //pulls '$cycle' amount of characters into its own array
        while ($cycle > 0){
            $keymade = $keymade.$array[$randmake[$postart]];  //actually creates the key
            $postart = $postart +1;
            $cycle = $cycle -1;
        }
        return $keymade;  //remove this after testing
    }

    public function uniqueCode(){
        $code = self::generateCode();
        $check = $this->_db->get('events', array('Public_Event_ID', '=', $code));

        while($check->count()> 0){
            //check db here through an abstraction I had made.
            //i need to have this generate a new code and check if it's valid, if it's not valid
            //make a new code, and test it again until the test passes.
            $code = self::generateCode();
            $check = $this->_db->get('events', array('Public_Event_ID', '=', $code));
        }
        return $code;
    }

    public function find($code = null) {
        if($code){
            $data = $this->_db->get('events', array('public_event_ID', '=', $code));

            if ($data->count()){
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }

    public function create($fields=array()){
        if(!$this->_db->insert('events', $fields)){
            throw new Exception('There was a problem creating your event.');
        }
    }

    public function findPartiesHosted($user_id=null){
        $search = $this->_db->get('events', array('creators_ID','=', $user_id));

        if($search){
            return $search->results();
        }
        return false;
    }

    public function data(){
        return $this->_data;
    }

    public function results(){
        return $this->_results();
    }

    public function count(){
        return $this->_count;
    }
}