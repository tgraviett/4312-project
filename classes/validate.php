<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:06 AM
 */
class Validate{
    private $_passed=false,
            $_errors=array(),
            $_db=null;
    public function __construct(){
        $this->_db = DB::getInstance();
    }


    /**
     * @Source is the post or get type
     * @items is an array of fields to validate
     * @rules is an array of rule values switched through, with each rule having a rule_value
     * that indicates the expected value of the response
     * @
     *
     */
    public function check($source, $items = array()){
        foreach($items as $item => $rules){
            foreach($rules as $rule => $rule_value){
                $value = trim($source[$item]);
                $item = escape($item);

                if ($rule === 'required' && empty($value)) {
                    $this->addError("{$item} is required");
                } else if(!empty($value)){

                    switch($rule){
                        case 'min':
                            if(strlen($value) < $rule_value){
                                $this->addError("{$item} must be a minimum of {$rule_value} characters.");
                            }
                            break;
                        case 'max':
                            if(strlen($value) > $rule_value){
                                $this->addError("{$item} cannot be more than the maximum of {$rule_value} characters.");
                            }
                            break;
                        case 'matches':
                            if($value!= $source[$rule_value]){
                                $this->addError("{$item} must match {$rule_value}");
                            }
                            break;
                        case 'unique':
                            $check = $this->_db->get($rule_value, array($item,'=',$value));
                            if($check->count()){
                               $this->addError("{$item} already exists.");
                            }
                            break;
                        case 'valid_email':
                            $valid_email = filter_var($value, FILTER_VALIDATE_EMAIL);
                            if ($valid_email == false){
                                $this->addError("{$value} is not a valid email address");
                            }
                            break;
                        case 'valid_date':
                            //set date format
                            $format = 'm/d/Y';
                            //check the $value submitted to see if it fits the submitted date format
                            $d = DateTime::createFromFormat($format, $value);
                            // add error to be flashed if it's not valid
                            if($d != true){
                                $this->addError("{$value} is not a valid date");
                            }
                            break;
                        case 'valid_end_date':
                            break;

                    }
                }

            }
        }
        if(empty($this->_errors)){
            $this->_passed = true;
        }
        return $this;
    }
    private function addError($error){
        $this->_errors[] = $error;
    }

    public function errors(){
        return $this->_errors;
    }

    public function passed(){
        return $this->_passed;
    }
}