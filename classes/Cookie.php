<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/26/13
 * Time: 1:03 AM
 */
class Cookie{
    public static function exists($name) {
        return (isset($_COOKIE[$name])) ? true : false;
    }

    public static function get($name) {
        //get a cookie
        return $_COOKIE[$name];
    }

    public static function put($name, $value, $expiry){
        //create a cookie
        if(setcookie($name, $value, time()+$expiry, '/')){
            return true;
        }
        return false;
    }

    public static function delete($name) {
        //delete cookie
        self::put($name, '', time()-1);
    }
}