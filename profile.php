<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/2/13
 * Time: 4:19 AM
 */
require_once'core/init.php';

if(!$email = Input::get('email')){
    Redirect::to('index.php');
} else{
    $user = new User($email);
    if(!$user->exists()){
        Redirect::to(404);
    } else {
        $data = $user->data();
    }
    ?>
    <h3><?php echo escape($data->email);?></h3>
    <p>email address: <?php echo escape($data->email);?></p>
    <?php
}

