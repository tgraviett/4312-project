<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 12/2/13
 * Time: 12:21 AM
 */

require_once('core/init.php');
include 'templates/header.php';
$user = new User();

if(!$user->isLoggedin()){
    Redirect::to('index.php');

}
if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'email' => array(
                'required' => true,
                'min' => 2,
                'max' => 50
            )
        ));

        if($validation->passed()) {

            try {

                $user->update(array(
                    'email' => Input::get('email')
                ));
                Redirect::to('index.php');
                //Session::flash('home', 'your details have been update.');

            } catch(Exception $e) {

                die($e->getMessage());
            }
        }else{
            foreach($validation->errors() as $error){
                echo $error,'<br>';
            }
        }
    }
?>
<form action="" method="post">
    <div class="field">
        <label for="email">Email</label>
        <input type="text" name="email" id="email" value="<?php echo escape ($user->data()->email);?>">
        <input type="submit" value="Update">
        <input type="hidden" name="token" value="<?php echo Token::generate();?>">
    </div>
</form>
<?php
include 'templates/changepassword.php';
}