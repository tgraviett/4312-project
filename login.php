<?php
/**
 * Project: sp.
 * Developer: Thomas Graviett
 * Date: 11/25/13
 * Time: 7:22 PM
 */
require_once 'core/init.php';
include 'templates/header.php';
if(Input::exists()){
    if(Token::check(Input::get('token'))){
        $validate = new Validate();
        echo Input::get('email');
        $validation = $validate ->check($_POST, array(
            'email' => array('required'=> true),
            'password' => array('required'=> true)
        ));

        if($validation->passed()){
            $user = new User();
            $remember = (Input::get('remember')=== 'on') ? true : false;
            $login = $user->login(Input::get('email'), Input::get('password'), $remember);

            if($login){
                Redirect::to('index.php');
            } else{
                Session::flash('error', 'Either your username or password was incorrect!');
                Redirect::to('login.php');
            }
        }
        } else {
            Session::flash('error', 'You have to enter credentials to be logged in!');
            Redirect::to('login.php');
        }
    }

?>
</head>
<body>
<div data-role="page" id="login">

    <div data-role="header">
        <h1>Login to the Party</h1>
        <br>
        <?php if(Session::exists('error')){
        echo '<div class="error">' .Session::flash('error').'</div>';
        }?>
        <br>
    </div>

    <div>

    </div>

    <div data-role="content">
        <form action="" method="POST">
            <input type="email" name="email" placeholder="E-mail" autocomplete="off"><br />
            <input type="password" name="password" placeholder="Password"><br />
            <input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
            <input type="checkbox" name="remember" value="w">
            <p class="spacer50"></P>
            <input type="submit" class="button3" value="Login">
        </form>
            <br>
            <li>
                <div class="button" onclick="window.location.href='register.php';" >Register</div>
            </li>
            <br>
        </ul>
    </div>
</body>
</html>